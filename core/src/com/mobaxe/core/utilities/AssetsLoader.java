package com.mobaxe.core.utilities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class AssetsLoader {

	public static Texture loadTexture(String fileName) {
		return new Texture(Gdx.files.internal(fileName));
	}

}
