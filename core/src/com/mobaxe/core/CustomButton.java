package com.mobaxe.core;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.checkers.MyScreens;
import com.mobaxe.checkers.ScreenManager;
import com.mobaxe.core.utilities.AssetsLoader;

public class CustomButton extends Button {

	public static String START = "start";
	public static String OPTIONS = "options";
	public static String EXIT = "exit";
	public static String FACEBOOK = "fbLogin";
	public static int clickCounter;

	private String buttonUp;
	private String buttonOver;
	private String buttonName;

	private Skin skin;
	private ButtonStyle style;

	public CustomButton(String buttonName, boolean isAnimatedButton) {
		this.buttonName = buttonName;
		buttonUp = buttonName + "ButtonUp";
		buttonOver = buttonName + "buttonOver";
		initSkins();
		setButtonStyles();
		clickListener(buttonName, isAnimatedButton);

	}

	private void clickListener(final String buttonName, final boolean isAnimatedButton) {
		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				clickCounter++;
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getStageX() > getX() && event.getStageX() < getX() + getWidth()
						&& event.getStageY() < getY() + getHeight() && event.getStageY() > getY()) {
					if (clickCounter == 1) {
						if (isAnimatedButton) {
							buttonAnimation();
						} else {
							buttonFunction();
						}
					}
				} else {
					clickCounter--;
				}
			}

			private void buttonFunction() {

				if (buttonName.equals(START)) {
					goToGameScreen();
				} else if (buttonName.equals(OPTIONS)) {
					optionsDialog();
				} else if (buttonName.equals(EXIT)) {
					exitDialog();
				} else if (buttonName.equals(FACEBOOK)) {
				}

				clickCounter = 0;
			}

			private void buttonAnimation() {
				addAction(Actions.sequence(Actions.moveTo(getX(), getY() - 20, 0.2f, Interpolation.sineOut),
						Actions.moveTo(getX(), getY() + 20, 0.2f, Interpolation.sineOut),
						Actions.moveTo(getX(), getY(), 0.4f, Interpolation.sineOut), Actions.run(new Runnable() {
							@Override
							public void run() {
								if (buttonName.equals(START)) {
									goToGameScreen();
								} else if (buttonName.equals(OPTIONS)) {
									optionsDialog();
								} else if (buttonName.equals(EXIT)) {
									exitDialog();
								} else if (buttonName.equals(FACEBOOK)) {
								}
								clickCounter = 0;
							}
						})));
			}
		});
	}

	private void initSkins() {
		skin = new Skin();

		if (buttonName.equals(START)) {
			skin.add(buttonUp, AssetsLoader.loadTexture("mainMenuButtons/startGameButton.png"));
			skin.add(buttonOver, AssetsLoader.loadTexture("mainMenuButtons/startGameButtonOver.png"));
		} else if (buttonName.equals(OPTIONS)) {
			skin.add(buttonUp, AssetsLoader.loadTexture("mainMenuButtons/optionsButton.png"));
			skin.add(buttonOver, AssetsLoader.loadTexture("mainMenuButtons/optionsButtonOver.png"));
		} else if (buttonName.equals(EXIT)) {
			skin.add(buttonUp, AssetsLoader.loadTexture("mainMenuButtons/exitButton.png"));
			skin.add(buttonOver, AssetsLoader.loadTexture("mainMenuButtons/exitButtonOver.png"));
		} else if (buttonName.equals(FACEBOOK)) {
			skin.add(buttonUp, AssetsLoader.loadTexture("mainMenuButtons/loginFb.png"));
			skin.add(buttonOver, AssetsLoader.loadTexture("mainMenuButtons/loginFbOver.png"));
		}
	}

	public void setButtonStyles() {
		style = new ButtonStyle();
		style.up = skin.getDrawable(buttonUp);
		style.over = skin.getDrawable(buttonOver);
		style.down = skin.getDrawable(buttonOver);
		setStyle(style);
	}

	private void goToGameScreen() {

		getParent().addAction(Actions.sequence(Actions.fadeOut(1.1f), Actions.run(new Runnable() {

			@Override
			public void run() {
				ScreenManager.getInstance().dispose(MyScreens.MAIN_MENU_SCREEN);
				ScreenManager.getInstance().show(MyScreens.GAME_SCREEN);
			}
		})));
	}

	private void exitDialog() {
		CustomDialog dialog = new CustomDialog(getStage(), true, false);
		dialog.show();
	}

	private void optionsDialog() {
		CustomDialog dialog = new CustomDialog(getStage(), false, true);
		dialog.show();
	}

}
