package com.mobaxe.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mobaxe.checkers.GameManager;

public class CustomDialog extends ScreenAdapter {

	private Skin skin;
	private Stage stage;
	private boolean closeGame;
	private boolean showOptions;

	public CustomDialog(Stage stage, boolean closeGame, boolean showOptions) {
		this.closeGame = closeGame;
		this.showOptions = showOptions;
		this.stage = stage;
		skin = new Skin(Gdx.files.internal("uiskin.json"));
	}

	@Override
	public void render(float delta) {
	}

	@Override
	public void show() {
		if (closeGame && showOptions == false) {
			exitGameDialog();
		} else if (showOptions && closeGame == false) {
			showOptionsDialog();
		}
	}

	private void exitGameDialog() {
		new Dialog("Confirm Exit", skin) {
			{
				text("Do you really want to exit ?");
				button("Yes !", GameManager.EXIT_GAME);
				button("No !", GameManager.CANCEL_EXIT_GAME);
			}

			@Override
			protected void result(Object object) {
				if (object == (Object) GameManager.EXIT_GAME) {
					GameManager.exitApplication();
				}
			}
		}.show(stage);
	}

	private void showOptionsDialog() {
		new Dialog("SOUND SETTINGS", skin) {
			{

				button("Sound ON");
				button("Sound OFF");
			}

			@Override
			protected void result(Object object) {

			}
		}.show(stage);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}

}