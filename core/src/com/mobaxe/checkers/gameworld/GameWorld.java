package com.mobaxe.checkers.gameworld;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.mobaxe.checkers.GameManager;
import com.mobaxe.checkers.MyScreens;
import com.mobaxe.checkers.ScreenManager;
import com.mobaxe.checkers.gameobjects.Cell;
import com.mobaxe.checkers.gameobjects.Checker;
import com.mobaxe.checkers.gameobjects.CheckersBoard;
import com.mobaxe.core.utilities.AssetsLoader;
import com.mobaxe.core.utilities.MyUtils;

public class GameWorld extends Stage implements InputProcessor {

	private CheckersBoard board;

	private Texture darkCheckerTexture;
	private Texture lightCheckerTexture;
	private Texture textureToDraw;

	private ArrayList<Checker> checkers;
	private ArrayList<Cell> cells;

	private Vector2 checkerPosition;
	private Vector2 cellPosition;

	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;

	private boolean runOnce = true;

	private int rowNumber = 1;

	public GameWorld() {
		setViewport(new StretchViewport(MyUtils.virtualWidth, MyUtils.virtualHeight));
		loadCheckersImage();
		board = new CheckersBoard();
		checkerPosition = new Vector2();
		cellPosition = new Vector2();
		cells = new ArrayList<Cell>();
		checkers = new ArrayList<Checker>();

		GameManager.setActivePlayer(GameManager.WHITE);
		GameManager.setBoard(board);
		GameManager.setWorld(this);
	}

	public void render(float delta) {
		act(delta);
		draw();
		if (runOnce) {
			GameManager.findFullCells();
			runOnce = false;
		}
	}

	public void show() {
		Gdx.input.setInputProcessor(this);
		Gdx.input.setCatchBackKey(true);

		setBackgroundImage();
		addActor(board);
		drawCellstoTheBoard();
		putCheckersToTheBoard();

	}

	public void resize(int width, int height) {
		getViewport().update(width, height, true);
	}

	@Override
	public boolean keyDown(int keyCode) {
		if (keyCode == Keys.BACK) {
			ScreenManager.getInstance().dispose(MyScreens.GAME_SCREEN);
			ScreenManager.getInstance().show(MyScreens.MAIN_MENU_SCREEN);
		}
		return false;
	}

	private void drawCellstoTheBoard() {

		for (int i = 0; i < 64; i++) {
			cellPosition.x = CheckersBoard.topSpace + ((i % 8) * CheckersBoard.cellSize);
			cellPosition.y = CheckersBoard.topSpace + ((i / 8) * CheckersBoard.cellSize);
			cells.add(new Cell(cellPosition.x, cellPosition.y, i));
			addActor(cells.get(i));
		}
	}

	private void putCheckersToTheBoard() {

		for (int i = 0; i < 32; i++) {
			if (i < 16) {
				checkerPosition.x = CheckersBoard.topSpace + ((i % 8) * CheckersBoard.cellSize);
				checkerPosition.y = CheckersBoard.topSpace + ((i / 8 + rowNumber) * CheckersBoard.cellSize);
				addObjects(i);
			} else {
				if (rowNumber == 1) {
					rowNumber = 3;
				}
				checkerPosition.x = CheckersBoard.topSpace + ((i % 8) * CheckersBoard.cellSize);
				checkerPosition.y = CheckersBoard.topSpace + ((i / 8 + rowNumber) * CheckersBoard.cellSize);
				addObjects(i);
			}
		}
	}

	private void setBackgroundImage() {
		bgTexture = AssetsLoader.loadTexture("mainmenuBackground.png");

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(MyUtils.virtualWidth / 2 - bgSprite.getWidth() / 2);
		bgSprite.setY(MyUtils.virtualHeight / 2 - bgSprite.getHeight() / 2);
		bgImage = new Image(bgSprite);

		addActor(bgImage);
	}

	private void addObjects(int i) {
		if (i < 16) {
			textureToDraw = darkCheckerTexture;
			checkers.add(new Checker(textureToDraw, checkerPosition.x, checkerPosition.y, i));
			checkers.get(i).setCheckerColor(GameManager.BLACK);
		} else {
			textureToDraw = lightCheckerTexture;
			checkers.add(new Checker(textureToDraw, checkerPosition.x, checkerPosition.y, i));
			checkers.get(i).setCheckerColor(GameManager.WHITE);
		}
		addActor(checkers.get(i));
	}

	private void loadCheckersImage() {
		darkCheckerTexture = AssetsLoader.loadTexture("checkerImages/dark_checker.png");
		lightCheckerTexture = AssetsLoader.loadTexture("checkerImages/light_checker.png");
	}

	public ArrayList<Checker> getCheckers() {
		return checkers;
	}

	public ArrayList<Cell> getCells() {
		return cells;
	}
}