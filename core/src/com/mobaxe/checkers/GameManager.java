package com.mobaxe.checkers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.mobaxe.checkers.gameobjects.Checker;
import com.mobaxe.checkers.gameobjects.CheckersBoard;
import com.mobaxe.checkers.gameworld.GameWorld;

public final class GameManager {

	public final static int DIR_NORTH = 0;
	public final static int DIR_SOUTH = 1;
	public final static int DIR_WEST = 2;
	public final static int DIR_EAST = 3;

	public static int EXIT_GAME = 0;
	public static int CANCEL_EXIT_GAME = 1;

	public final static int WHITE = 0;
	public final static int BLACK = 1;

	public static int selectedCheckerID;
	public static Vector2 selectedCell = new Vector2();
	public static int selectedCellID;

	private static int activePlayer;
	private static Checker selectedChecker;
	private static CheckersBoard board;
	private static GameWorld world;

	private static Rectangle cell;
	private static Rectangle checker;

	public static HashMap<Integer, Integer> fullCells = new HashMap<Integer, Integer>();
	public static boolean isMovingSucceed = false;

	public static void makeMove() {
		if (getSelectedChecker() == null) {
			return;
		}
		findAndSetMovingDirection();

		switch (getSelectedChecker().getDirection()) {
		case DIR_NORTH:
			if (activePlayer == WHITE) {
				if (canMoveBackward() && isKingPlayable()) {
					moveChecker(0, -1);
				}
			} else {
				if (darkCanMoveForward()) {
					moveChecker(0, -1);
				}
			}
			break;
		case DIR_SOUTH:
			if (activePlayer == WHITE) {
				if (canMoveForward()) {
					moveChecker(0, -1);
				}
			} else {
				if (canMoveBackward() && isKingPlayable()) {
					moveChecker(0, -1);
				}
			}

			break;
		case DIR_WEST:
			if (canMoveLeft()) {
				moveChecker(0, -1);
			}
			break;
		case DIR_EAST:
			if (canMoveRight()) {
				moveChecker(0, -1);
			}
			break;
		}
	}

	// D�NAM�K YAPILMALI
	public static void findFullCells() {

		for (int i = 0; i < 64; i++) {
			for (int k = 0; k < 32; k++) {
				checker = getWorld().getCheckers().get(k).getRectangle();
				cell = getWorld().getCells().get(i).getRectangle();
				if (checker.overlaps(cell)) {
					fullCells.put(k, i);
					// System.out.println(String.format("Checker ID: %d ---- Full Cell ID: %d ",
					// k, i));

				}
			}
		}
	}

	public static int getKeyOfValue(int value) {

		for (int i = 0; i < 64; i++) {
			Iterator<Entry<Integer, Integer>> iter = fullCells.entrySet().iterator();
			while (iter.hasNext()) {
				Entry<Integer, Integer> entry = iter.next();
				if (entry.getValue() == value) {
					return entry.getKey();
				}
			}
		}
		return -1;
	}

	public static boolean canJump() {

		// System.err.println(getKeyOfValue(fullCells.get(selectedCheckerID) -
		// 8));

		int checkerID = getKeyOfValue(fullCells.get(selectedCheckerID) - 8);

		if (getKeyOfValue(fullCells.get(selectedCheckerID) - 8) != -1 && checkerID >= 0 && checkerID < 16
				&& selectedCheckerID >= 16 && selectedCheckerID < 32) {
			moveChecker(1, selectedCheckerID);
		} else {
		}
		return true;
	}

	private static void findAndSetMovingDirection() {

		if (selectedCell.y < getSelectedChecker().getY()) {
			getSelectedChecker().setDirection(DIR_SOUTH);
		} else if (selectedCell.y > getSelectedChecker().getY()) {
			getSelectedChecker().setDirection(DIR_NORTH);
		} else if (selectedCell.x > getSelectedChecker().getX()) {
			getSelectedChecker().setDirection(DIR_EAST);
		} else if (selectedCell.x < getSelectedChecker().getX()) {
			getSelectedChecker().setDirection(DIR_WEST);
		}
	}

	private static void moveChecker(int canEatBlackChecker, int checkerID) {
		// 0 is null
		// 1 is blackChecker can be eaten
		// 2 is whiteChecker can be eaten

		setSelectedChecker(world.getCheckers().get(selectedCheckerID));
		if (canEatBlackChecker == 1 && checkerID >= 16 && checkerID < 32) {
			System.err.println("1--------- 16 and 32 ");
			if (getSelectedChecker().getY() == selectedCell.y + 128 && getSelectedChecker().getX() == selectedCell.x) {
				getSelectedChecker().addAction(Actions.moveTo(selectedCell.x, selectedCell.y, 0.3f));
				isMovingSucceed = true;
				System.err.println("y = selected y + 128  ---- x = selected x");

			}else{
				isMovingSucceed = false;
			}
		} else if (canEatBlackChecker == 0) {
			getSelectedChecker().addAction(Actions.moveTo(selectedCell.x, selectedCell.y, 0.3f));
			isMovingSucceed = true;
			System.err.println("ELSE if");
		} else {
			isMovingSucceed = false;
			System.err.println("ELSE");
		}
		if (isMovingSucceed) {
			getSelectedChecker().toFront();
			isKingController();
		}
	}

	private static boolean canMoveForward() {
		if ((getSelectedChecker().isKing() || getSelectedChecker().getY() - selectedCell.y == CheckersBoard.cellSize)
				&& getSelectedChecker().getX() == selectedCell.x) {
			return true;
		}
		return false;
	}

	private static boolean darkCanMoveForward() {
		if ((getSelectedChecker().isKing() || getSelectedChecker().getY() - selectedCell.y == -CheckersBoard.cellSize)
				&& getSelectedChecker().getX() == selectedCell.x) {
			return true;
		}
		return false;
	}

	private static boolean canMoveBackward() {
		return getSelectedChecker().isKing();
	}

	private static boolean isKingPlayable() {
		if (selectedCell.x == getSelectedChecker().getX() || selectedCell.y == getSelectedChecker().getY()) {
			return true;
		}
		return false;
	}

	private static boolean canMoveRight() {
		if ((selectedCell.x - CheckersBoard.cellSize == getSelectedChecker().getX() || getSelectedChecker().isKing())
				&& selectedCell.y == getSelectedChecker().getY()) {
			return true;
		}
		return false;
	}

	private static boolean canMoveLeft() {
		if ((selectedCell.x + CheckersBoard.cellSize == getSelectedChecker().getX() || getSelectedChecker().isKing())
				&& selectedCell.y == getSelectedChecker().getY()) {
			return true;
		}
		return false;
	}

	private static void isKingController() {
		if (!getSelectedChecker().isKing()) {
			if ((selectedCell.x >= 44 && selectedCell.x <= 492) && selectedCell.y == 492 || selectedCell.y == 44) {
				getSelectedChecker().setKing(true);
				getSelectedChecker().changeKingImage(selectedCheckerID);
			}
		}
	}

	public static void exitApplication() {
		Gdx.app.exit();
	}

	public static void switchActivePlayer() {
		if (activePlayer == WHITE) {
			activePlayer = BLACK;
		} else {
			activePlayer = WHITE;
		}
	}

	public static Checker getSelectedChecker() {
		return selectedChecker;
	}

	public static void setSelectedChecker(Checker selectedChecker) {
		GameManager.selectedChecker = selectedChecker;
	}

	public static int getActivePlayer() {
		return activePlayer;
	}

	public static void setActivePlayer(int active) {
		activePlayer = active;
	}

	public static CheckersBoard getBoard() {
		return board;
	}

	public static void setBoard(CheckersBoard board) {
		GameManager.board = board;
	}

	public static GameWorld getWorld() {
		return world;
	}

	public static void setWorld(GameWorld world) {
		GameManager.world = world;
	}

}
