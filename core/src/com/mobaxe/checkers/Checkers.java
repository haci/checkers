package com.mobaxe.checkers;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.mobaxe.core.utilities.MyUtils;

public class Checkers extends Game {

	@Override
	public void create() {
		MyUtils.gameWidth = Gdx.graphics.getWidth();
		MyUtils.gameHeight = Gdx.graphics.getHeight();

		ScreenManager.getInstance().initialize(this);
		ScreenManager.getInstance().show(MyScreens.MAIN_MENU_SCREEN);
	}

	@Override
	public void dispose() {
		super.dispose();
		ScreenManager.getInstance().dispose();
	}

}
