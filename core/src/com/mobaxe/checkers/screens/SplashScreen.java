//package com.mobaxe.checkers.screens;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.Screen;
//import com.badlogic.gdx.graphics.GL20;
//import com.badlogic.gdx.math.Interpolation;
//import com.badlogic.gdx.scenes.scene2d.Stage;
//import com.badlogic.gdx.scenes.scene2d.actions.Actions;
//import com.badlogic.gdx.scenes.scene2d.ui.Image;
//import com.badlogic.gdx.utils.viewport.FillViewport;
//import com.mobaxe.checkers.ScreenManager;
//import com.mobaxe.core.MyScreens;
//import com.mobaxe.core.utilities.AssetsLoader;
//import com.mobaxe.core.utilities.MyUtils;
//
//public class SplashScreen implements Screen {
//
//	private Stage stage;
//	private Image logo;
//
//	public SplashScreen() {
//		logo = new Image(AssetsLoader.loadTexture("picarexLogo.png"));
//		stage = new Stage(new FillViewport(MyUtils.virtualWidth, MyUtils.virtualHeight));
//	}
//
//	@Override
//	public void render(float delta) {
//		Gdx.gl.glClearColor(1, 1, 1, 1);
//		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//
//		stage.act();
//		stage.draw();
//	}
//
//	Runnable onActionEnd = new Runnable() {
//		@Override
//		public void run() {
//			ScreenManager.getInstance().dispose(MyScreens.SPLASH_SCREEN);
//			ScreenManager.getInstance().show(MyScreens.MAIN_MENU_SCREEN);
//		}
//	};
//
//	@Override
//	public void show() {
//		logo.setPosition(MyUtils.virtualWidth / 2.7f, MyUtils.virtualHeight / 1f);
//		logo.addAction(Actions.sequence(Actions.moveTo(logo.getX(), logo.getY() - 400, 1.5f, Interpolation.bounceOut)));
//
//		stage.addActor(logo);
//		stage.addAction(Actions.sequence(Actions.delay(1.7f), Actions.fadeOut(1.7f, Interpolation.fade), Actions.run(onActionEnd)));
//
//	}
//
//	@Override
//	public void resize(int width, int height) {
//		stage.getViewport().update(width, height, true);
//	}
//
//	@Override
//	public void hide() {
//	}
//
//	@Override
//	public void pause() {
//	}
//
//	@Override
//	public void resume() {
//	}
//
//	@Override
//	public void dispose() {
//	}
//
//}