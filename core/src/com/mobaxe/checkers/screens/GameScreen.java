package com.mobaxe.checkers.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.mobaxe.checkers.gameworld.GameWorld;
import com.mobaxe.core.CustomScreen;

public class GameScreen extends CustomScreen {

	private GameWorld world;

	public GameScreen() {
		world = new GameWorld();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.6f, 0.6f, 0.6f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		world.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		world.resize(width, height);

	}

	@Override
	public void show() {
		world.show();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}
}
