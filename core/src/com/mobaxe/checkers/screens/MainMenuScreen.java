package com.mobaxe.checkers.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.mobaxe.core.CustomButton;
import com.mobaxe.core.CustomScreen;
import com.mobaxe.core.utilities.AssetsLoader;
import com.mobaxe.core.utilities.MyUtils;

public class MainMenuScreen extends CustomScreen {

	private Stage stage;
	private Table table;
	private Table bgTable;
	private Texture bgTexture;
	private Sprite bgSprite;
	private Image bgImage;

	private CustomButton startButton;
	private CustomButton optionsButton;
	private CustomButton exitButton;
	private CustomButton fbLoginButton;

	public MainMenuScreen() {
		stage = new Stage(new FillViewport(MyUtils.virtualWidth, MyUtils.virtualHeight));
		table = new Table();
		bgTable = new Table();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);

	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(true);
		setBackgroundImage();
		createButtons();
	}

	private void createButtons() {
		table.setFillParent(true);
		bgTable.setFillParent(true);

		startButton = new CustomButton(CustomButton.START, true);
		table.add(startButton).pad(0, 20, 0, 0);

		optionsButton = new CustomButton(CustomButton.OPTIONS, true);
		table.add(optionsButton).pad(0, 20, 0, 0);

		exitButton = new CustomButton(CustomButton.EXIT, true);
		table.add(exitButton).pad(0, 20, 0, 0);

		fbLoginButton = new CustomButton(CustomButton.FACEBOOK, true);
		table.add(fbLoginButton).pad(0, 20, 0, 0);

		bgImage = new Image(bgTexture);
		bgTable.add(bgImage);

		stage.addActor(bgTable);
		stage.addActor(table);
	}

	private void setBackgroundImage() {
		bgTexture = AssetsLoader.loadTexture("mainmenuBackground.png");

		bgSprite = new Sprite(bgTexture);
		bgSprite.setColor(0, 0, 0, 0);
		bgSprite.setX(MyUtils.virtualWidth / 2 - bgSprite.getWidth() / 2);
		bgSprite.setY(MyUtils.virtualHeight / 2 - bgSprite.getHeight() / 2);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
	}
}