package com.mobaxe.checkers.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.mobaxe.core.utilities.AssetsLoader;
import com.mobaxe.core.utilities.MyUtils;

public class CheckersBoard extends Actor {

	private Texture brownSquare;
	private Texture whiteSquare;
	private Texture textureToDraw;

	private int cellPosX;
	private int cellPosY;

	public static int cellSize;
	public static int topSpace;

	public CheckersBoard() {
		cellSize = 64;
		topSpace = (int) ((MyUtils.virtualHeight - (cellSize * 8)) / 2);
		loadTextures();

	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		renderCheckersBoard(batch);

	}

	public void renderCheckersBoard(Batch batch) {

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				cellPosX = topSpace + (j * cellSize);
				cellPosY = topSpace + (i * cellSize);
				if ((i + j - 1) % 2 == 0) {
					textureToDraw = whiteSquare;
				} else {
					textureToDraw = brownSquare;
				}
				batch.draw(textureToDraw, cellPosX, cellPosY);

			}
		}

	}

	private void loadTextures() {
		brownSquare = AssetsLoader.loadTexture("wood_brown_pattern.png");
		whiteSquare = AssetsLoader.loadTexture("wood_white_pattern.png");
	}
}
