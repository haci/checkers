package com.mobaxe.checkers.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mobaxe.checkers.GameManager;
import com.mobaxe.core.utilities.AssetsLoader;

public class Checker extends Actor {

	private int checkerColor;

	protected Texture checkerTexture;
	protected Texture kingCheckerTexture;
	protected Texture activeImage;
	protected Texture passiveImage;
	protected Texture passiveImageforKing;
	protected Texture activeImageforKing;

	protected boolean isHighlighted;

	private int direction;
	private boolean isKing = false;

	private Rectangle rectangle = new Rectangle();

	public Checker(Texture texture, float x, float y, int id) {
		setWidth(texture.getWidth());
		setHeight(texture.getHeight());
		setBounds(x, y, texture.getWidth(), texture.getHeight());
		setPosition(x, y);
		this.checkerTexture = texture;
		checkerClickListener(id);
	}

	private void checkerClickListener(final int id) {

		addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				systemOut(id, event);
				hightlightAndActivePlayerControl();
				if (!Cell.isClickable) {
					Cell.isClickable = true;
				}
				if (GameManager.selectedCheckerID == id) {
					GameManager.setSelectedChecker(null);
				} else {
					GameManager.selectedCheckerID = id;
				}
			}

			private void systemOut(final int id, InputEvent event) {
				System.out.println(String.format("CHECKER CLICKED target X: %f and target Y: %f ID: %d", event
						.getTarget().getX(), event.getTarget().getY(), id));
			}

			private void hightlightAndActivePlayerControl() {

				if (GameManager.getActivePlayer() != checkerColor) {
					return;
				}

				if (isHighlighted) {
					switchHighlight();
				} else {
					Timer.schedule(new Task() {
						@Override
						public void run() {
							switchHighlight();
						}
					}, 0, 0.05f, 4);
				}
			}
		});
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (!isKing) {
			batch.draw(checkerTexture, getX(), getY());
		} else {
			batch.draw(kingCheckerTexture, getX(), getY());
		}
		rectangle.set(getX() + 5, getY() + 5, 54, 54);

	}

	public void switchHighlight() {
		if (GameManager.getSelectedChecker() != null && GameManager.getSelectedChecker() != this) {
			GameManager.getSelectedChecker().setHighLight(false);
		}

		setHighLight(!isHighlighted);
	}

	public void setHighLight(boolean shouldHighlight) {
		isHighlighted = shouldHighlight;
		if (!isKing) {
			if (shouldHighlight) {
				checkerTexture = activeImage;
				GameManager.setSelectedChecker(this);
			} else {
				checkerTexture = passiveImage;
				GameManager.setSelectedChecker(null);
			}
		} else {
			if (shouldHighlight) {
				kingCheckerTexture = activeImageforKing;
				GameManager.setSelectedChecker(this);
			} else {
				kingCheckerTexture = passiveImageforKing;
				GameManager.setSelectedChecker(null);
			}
		}
	}

	public void changeKingImage(int id) {
		if (id < 16) {
			kingCheckerTexture = AssetsLoader.loadTexture("checkerImages/dark_checker_king.png");
		} else {
			kingCheckerTexture = AssetsLoader.loadTexture("checkerImages/light_checker_king.png");
		}
	}

	public void setCheckerColor(int checkerColor) {
		this.checkerColor = checkerColor;
		if (!isKing) {
			if (checkerColor == GameManager.WHITE) {
				activeImage = AssetsLoader.loadTexture("checkerImages/light_checker_clicked.png");
				passiveImage = AssetsLoader.loadTexture("checkerImages/light_checker.png");
				activeImageforKing = AssetsLoader.loadTexture("checkerImages/light_checker_king_clicked.png");
				passiveImageforKing = AssetsLoader.loadTexture("checkerImages/light_checker_king.png");
			} else {
				activeImage = AssetsLoader.loadTexture("checkerImages/dark_checker_clicked.png");
				passiveImage = AssetsLoader.loadTexture("checkerImages/dark_checker.png");
				activeImageforKing = AssetsLoader.loadTexture("checkerImages/dark_checker_king_clicked.png");
				passiveImageforKing = AssetsLoader.loadTexture("checkerImages/dark_checker_king.png");
			}
		}
	}

	public int getDirection() {
		return direction;
	}

	public void setDirection(int direction) {
		this.direction = direction;
	}

	public boolean getHighLight() {
		return isHighlighted;
	}

	public boolean isKing() {
		return isKing;
	}

	public void setKing(boolean isKing) {
		this.isKing = isKing;
	}

	public Rectangle getRectangle() {
		return rectangle;
	}

}
