package com.mobaxe.checkers.gameobjects;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mobaxe.checkers.GameManager;

public class Cell extends Actor {

	private Rectangle rectangle;

	public static boolean isClickable = false;

	public Cell(float x, float y, int id) {
		rectangle = new Rectangle();
		rectangle.set(x, y, CheckersBoard.cellSize, CheckersBoard.cellSize);

		setWidth(CheckersBoard.cellSize);
		setHeight(CheckersBoard.cellSize);
		setBounds(x, y, CheckersBoard.cellSize, CheckersBoard.cellSize);

		cellClickListener(id);
	}

	private void cellClickListener(final int id) {
		addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (isClickable) {
					GameManager.selectedCell.x = event.getTarget().getX();
					GameManager.selectedCell.y = event.getTarget().getY();
					GameManager.selectedCellID = id;
					GameManager.makeMove();
					GameManager.canJump();
					if (GameManager.isMovingSucceed) {
						GameManager.switchActivePlayer();
						updateEmptyCells();
						GameManager.isMovingSucceed = false;
					}
					isClickable = false;
					systemOut(id, event);
					if (GameManager.getSelectedChecker() != null) {
						GameManager.getSelectedChecker().switchHighlight();
					}
				}
			}

			private void updateEmptyCells() {
				Timer.schedule(new Task() {

					@Override
					public void run() {
						GameManager.findFullCells();
					}
				}, 0.4f);
			}

			private void systemOut(final int id, InputEvent event) {
				System.out.println(String.format("CELL CLICKED target X: %f and target Y: %f ID: %d", event.getTarget()
						.getX(), event.getTarget().getY(), id));
			}
		});
	}

	public Rectangle getRectangle() {
		return rectangle;
	}
}
