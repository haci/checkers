package com.mobaxe.checkers;

import com.badlogic.gdx.Screen;
import com.mobaxe.checkers.screens.GameScreen;
import com.mobaxe.checkers.screens.MainMenuScreen;

public enum MyScreens {

	MAIN_MENU_SCREEN {
		public Screen getScreenInstance() {
			return new MainMenuScreen();
		}
	},
	GAME_SCREEN {
		public Screen getScreenInstance() {
			return new GameScreen();
		}
	};

	public abstract Screen getScreenInstance();

}
