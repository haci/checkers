package com.mobaxe.worm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class Worm {

	private TextureAtlas textureAtlas = new TextureAtlas(Gdx.files.internal("worms/wormImages.atlas"));
	private SpriteBatch batch = new SpriteBatch();
	private Animation animation;

	private float gameWidth = Gdx.graphics.getWidth();
	private float textureWidth = textureAtlas.getRegions().get(0).originalWidth;
	private float x = 0;
	private float y = 0;
	private float elapsedTime;
	private float velocity;

	public Worm() {
		animation = new Animation(0.25f, textureAtlas.getRegions());
	}

	public Worm(float velocity, float animationSpeed, float x, float y) {
		this.x = x;
		this.y = y;
		this.velocity = velocity;
		animation = new Animation(animationSpeed, textureAtlas.getRegions());
	}

	public void render() {
		elapsedTime += Gdx.graphics.getDeltaTime();
		batch.begin();
		if (elapsedTime * velocity > gameWidth + textureWidth) {
			elapsedTime = 0;
			x = -textureWidth;
		}
		batch.draw(animation.getKeyFrame(elapsedTime, true), x + (elapsedTime * velocity), y);
		batch.end();
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getVelocity() {
		return velocity;
	}

	public void setVelocity(float velocity) {
		this.velocity = velocity;
	}

	public float getTextureWidth() {
		return textureWidth;
	}

	public void setAnimationSpeed(float animationSpeed) {
		animation = new Animation(animationSpeed, textureAtlas.getRegions());
	}

	public void dispose() {
		textureAtlas.dispose();
		batch.dispose();
	}

}
