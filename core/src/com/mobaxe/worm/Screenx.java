package com.mobaxe.worm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class Screenx implements Screen {

	private Worm worm;

	public Screenx() {
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		worm.render();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
		worm = new Worm(75, 0.25f, 0, 100);
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		worm.dispose();
	}

}
